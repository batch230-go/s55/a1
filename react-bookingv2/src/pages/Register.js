import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

    const { user } = useContext(UserContext);
    const navigate = useNavigate();
  
    //State hooks to store the values of input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [mobileNumber, setmobileNumber] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(email);
    console.log(password);

    const registerUser = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"

            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log("Registration Successful");
            console.log(data);
            if(data){
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Thank you for registering"
                })
                navigate("/login");
            }
            else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }
        })    
    }

    useEffect(() => {
         if(firstName !== '' && lastName !== '' && email !== '' && password !== '' && mobileNumber !== ''){
             setIsActive(true);
         }
         else{
             setIsActive(false);
         }
    }, [firstName, lastName, email, password, mobileNumber])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <div className='container-fluid d-flex justify-content-center'>
            <Form onSubmit={(event) => registerUser(event)}>
                <h2 className='pt-3'>Register</h2>
                <Form.Group controlId="firstName" className='pt-2'>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="firstName" 
                        placeholder="First Name" 
                        value = {firstName}
                        onChange = {event => setFirstName(event.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="lastName" className='pt-2'>
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="lastName" 
                        placeholder="Last Name" 
                        value = {lastName}
                        onChange = {event => setLastName(event.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="userEmail" className='pt-2'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password" className='pt-2'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password}
                        onChange = {event => setPassword(event.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="mobileNumber" className='pt-2'>
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="mobileNumber" 
                        placeholder="Mobile Number"
                        value = {mobileNumber}
                        onChange = {event => setmobileNumber(event.target.value)}
                        required
                    />
                </Form.Group>

                <div className='pt-3'>
                    { isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }
                </div>
            </Form>
        </div>
    )
}
