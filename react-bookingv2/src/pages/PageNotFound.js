
import React from 'react';
// import { Link } from 'react-router-dom';

/*
export default function NotFound(){
    
    return (
        <div>
            <h3>Page Not Found</h3>
            <p>Go back to the <Link to="/">homepage</Link>.</p>
        </div>
    )
}
*/
import Banner from '../components/Banner';

export default function NotFound(){

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }

    return (
        <Banner data={data}/>
    )
}

