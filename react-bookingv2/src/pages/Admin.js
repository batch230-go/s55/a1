import { Navigate } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function Admin() {

    const { user, setUser } = useContext(UserContext);

    return (
        (user.id !== null && user.admin)?
        <h3>Admin Page</h3>
        :
        <Navigate to="/login" />
    )
}