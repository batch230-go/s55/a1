import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login(){

    // 3 use the state
    // variable, setter function
    const { user, setUser } = useContext(UserContext);
    console.log(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [visible, setVisible] = useState(false);
    const [isActive, setIsActive] = useState(false);
    
    console.log(email);
    console.log(password);

    // function loginUser(event){
    //     event.preventDefault();
    //     localStorage.setItem('email', email); // s53

    //     setUser({
    //         email: localStorage.getItem('email')
    //     })

    //     setEmail('');
    //     setPassword('');
    //     alert('Login Successful');
    //     console.log(`${email} has been successfully login`);
    // }

    // another authenticate login
    function loginUser(e){
        e.preventDefault();
        fetch(`http://localhost:4000/users/login`, {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);

            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })
        setEmail('');
    }

    const retrieveUserDetails = (token) => {
       
        fetch(`http://localhost:4000/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            localStorage.setItem('email', data.email);
            localStorage.setItem('id', data._id);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                email: data.email
            })
        })
    }


    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])

    return(
        
        (user.id !== null && user.email !== null)? // true - it means that id field is successfully set
        <Navigate to ="/courses" />
        :
        <div className='container-fluid d-flex justify-content-center'>
            <Form onSubmit={(event) => loginUser(event)}>
            <h2 className='pt-3'>Login</h2>
                <Form.Group controlId="userEmail" className='pt-2'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="password" className='pt-2'>
                    <Form.Label>Password</Form.Label>
                    <div className='d-flex'>
                        <Form.Control 
                            type={visible? "text" : "password"}
                            placeholder="Password" 
                            value = {password}
                            onChange = {e => setPassword(e.target.value)}
                            className = ''
                            required
                        ></Form.Control>
                        <div className='p-2' onClick={() => setVisible(!visible)}>
                            {
                                visible? <EyeOutlined /> : <EyeInvisibleOutlined />
                            }
                        </div>
                    </div>
                </Form.Group>

                <div className='pt-3'>
                    { isActive ?
                        <Button variant="success" type="submit" id="submitBtn">
                            Login
                        </Button>
                        :
                        <Button variant="success" type="submit" id="submitBtn" disabled>
                            Login
                        </Button>
                    }  
                </div>
            </Form>
        </div> 
    )
}
